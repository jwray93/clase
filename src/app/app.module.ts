import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { MapModalComponent } from './shared/map-modal/map-modal.component';
import { LocationPickerComponent } from './shared/pickers/location-picker/location-picker.component';
export const firebaseConfig = {
  apiKey: 'AIzaSyASlHgzPIg8iWGKBTkMTLCcpxl-edU3GEA',
  authDomain: 'corded-shard-258305.firebaseapp.com',
  databaseURL: 'https://corded-shard-258305.firebaseio.com',
  projectId: 'corded-shard-258305',
  storageBucket: 'corded-shard-258305.appspot.com',
  messagingSenderId: '634903676430',
  appId: '1:634903676430:web:69f13ca3a9912d0bfcde5f',
  measurementId: 'G-3S0VY7HYVC'
};
@NgModule({
  declarations: [AppComponent,  MapModalComponent,
    LocationPickerComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    FormsModule,
    AngularFireModule,
    AngularFirestoreModule,
    AngularFireModule.initializeApp(firebaseConfig)
    ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
