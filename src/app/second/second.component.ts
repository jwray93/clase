import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-personas',
  templateUrl: './second.component.html',
  styleUrls: ['./second.component.scss'],
})
export class SecondComponent implements OnInit {
  @Input() personasSecond: string[];
  constructor() { }

  ngOnInit() {}

}
