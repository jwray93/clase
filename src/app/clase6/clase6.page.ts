import { Component, OnInit } from '@angular/core';
import { clase6 } from './clase6.model';

@Component({
  selector: 'app-clase6',
  templateUrl: './clase6.page.html',
  styleUrls: ['./clase6.page.scss'],
})
export class Clase6Page implements OnInit {
  usuarios: clase6 [] = [
    {
     id: 0,
     name: 'Jorge',
     last_name: 'Wray',
    },
    {
      id: 1,
      name: 'Diana',
      last_name: 'Hernandez',
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
