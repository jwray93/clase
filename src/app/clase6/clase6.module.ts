import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Clase6PageRoutingModule } from './clase6-routing.module';

import { Clase6Page } from './clase6.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Clase6PageRoutingModule
  ],
  declarations: [Clase6Page]
})
export class Clase6PageModule {}
