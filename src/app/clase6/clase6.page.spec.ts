import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Clase6Page } from './clase6.page';

describe('Clase6Page', () => {
  let component: Clase6Page;
  let fixture: ComponentFixture<Clase6Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Clase6Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Clase6Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
