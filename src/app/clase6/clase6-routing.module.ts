import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Clase6Page } from './clase6.page';

const routes: Routes = [
  {
    path: '',
    component: Clase6Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Clase6PageRoutingModule {}
