import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { MapModalComponent } from '../../map-modal/map-modal.component';
import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http'; 
import { environment } from '../../../../environments/environment';
import { map, switchMap } from 'rxjs/operators';
import { placeLocation } from './location-picker.model';
import { of } from 'rxjs';

@Component({
  selector: 'app-location-picker',
  templateUrl: './location-picker.component.html',
  styleUrls: ['./location-picker.component.scss'],
})
export class LocationPickerComponent implements OnInit {
  selectedLocationImage: string;
  constructor(
    private modal: ModalController,
    private http: HttpClient) {
   }
  onPickLocation(){
    this.modal.create({
      component: MapModalComponent
    }).then(
      modalEl => {
        modalEl.onDidDismiss().then(
          seleted => {
            if ( !seleted.data ){
              return;
            }
            const pickerlocation: placeLocation = {
              lat: seleted.data.lat,
              lng: seleted.data.lng,
              address: null,
              staticMapImageUrl: null
            };
            this.getAddress(seleted.data.lat, seleted.data.lng).pipe(
              switchMap(address => {
                pickerlocation.address = address;
                return of(this.getMapImage(seleted.data.lat, seleted.data.lng, 14));
              })
            ).subscribe(
              staticMapImage => {
                pickerlocation.staticMapImageUrl = parse(staticMapImage);
                this.selectedLocationImage = staticMapImage;
              }
            );
          }
        );
        modalEl.present();
      }
    );
  }
  ngOnInit() {}
  private getAddress(lat: number, lng: number){
    return this.http.get <any>(
      `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=${environment.googleMapApi}`
    ).pipe(map(geodata => {
      if (!geodata){
        return null;
      }
      return geodata.results[0].formatted_address;
    }));
  }
  private getMapImage(lat: number, lng: number, zoom: number){
    return 
    `https://maps.googleapis.com/maps/api/staticmap?center=${lat},${lng}&zoom=${zoom}&size=500x300&maptype=roadmap
    &markers=color:red%7Clabel:C%7C${lat},${lng}
    &key=${environment.googleMapApi}`;
  }
}
