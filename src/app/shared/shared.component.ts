import { Component, OnInit } from '@angular/core';
import { LocationPickerComponent } from './pickers/location-picker/location-picker.component';

@Component({
  selector: 'app-shared',
  templateUrl: './shared.component.html',
  styleUrls: ['./shared.component.scss'],
})
export class SharedComponent implements OnInit {

  constructor() { }

  ngOnInit() {}

}
