import { Component, OnInit } from '@angular/core';
import { receta } from './recetas.model';
import { ServiceService } from './service.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-recetas',
  templateUrl: './recetas.page.html',
  styleUrls: ['./recetas.page.scss'],
})
export class RecetasPage implements OnInit {
  private recetas: Observable<receta[]>;
  constructor(private servicio: ServiceService) { }

  ngOnInit() {
    console.log('On Init');
  }
  ionViewWillEnter(){
    this.recetas = this.servicio.getAllRecetas();
    console.log('Will Enter');
  }
  ionViewDidEnter(){
    console.log('Did Enter');
  }
  ionViewWillLeave(){
    console.log('Will leave');
  }
  ionViewDidLeave(){
    console.log('Did  leave');
  }
}
