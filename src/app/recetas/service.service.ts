import { Injectable } from '@angular/core';
import { receta } from './recetas.model';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { AngularFirestore } from 'angularfire2/firestore';
import { map } from 'rxjs/operators';
import { Title } from '@angular/platform-browser';
import { AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  private recetas: Observable<receta[]>;
  private recetasref: AngularFirestoreCollection<receta>;
  constructor(
    private afs: AngularFirestore
  ) {
  }
  getSongList(): AngularFirestoreCollection<receta> {
    return this.afs.collection('receta');
  }
  getAllRecetas(){
    this.recetasref = this.afs.collection('receta');
    console.log(this.recetasref.snapshotChanges());
    console.log('h');
    this.recetas = this.recetasref.valueChanges();
    console.log( this.recetas);
    return this.recetas;
  }
  addReceta(ptitle: string, pdescripcion: string, pimagen: string, pingredientes: string ){
    return new Promise<any>((resolve, reject) => {
      this.afs.collection('/receta').add({
        descripcion: pdescripcion,
        title: ptitle,
        image: pimagen,
        ingredients: pingredientes,
      })
      .then((res) => { resolve(res); }, err => reject(err));
    });
  }
}
