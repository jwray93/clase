import { Component, OnInit } from '@angular/core';
import { receta } from '../recetas.model';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceService } from '../service.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.page.html',
  styleUrls: ['./detalle.page.scss'],
})
export class DetallePage implements OnInit {
  rectaActual: receta;

  constructor(
    private route: ActivatedRoute,
    private servicio: ServiceService,
    private router: Router,
    private alert: AlertController
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap => {
      if (!paramMap.has('recetaId')){
        return;
      }
      const recetaID = paramMap.get('recetaId');
    });
  }
  deleteReceta(){
    this.alert.create({
      header: 'Seguro que desea borrar la receta',
      message: 'Realmente deseas borrar la receta',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Borrar',
          handler: () => {
            this.router.navigate(['/recetas']);
          }
        }
      ]
    })
    .then(alertEl => {
      alertEl.present();
    });
  }

}
