import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.page.html',
  styleUrls: ['./agregar.page.scss'],
})
export class AgregarPage implements OnInit {
 form: FormGroup;
  constructor(
    private servicio: ServiceService
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      title: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required]
      }),
      descripcion: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required]
      }),
      ingredientes: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required]
      }),
      imagen: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required]
      }),
      location: new FormControl(null, {
        validators: [Validators.required]
      }),
    });
  }
  onClick(){
    this.servicio.addReceta(
      this.form.value.title,
      this.form.value.descripcion,
      this.form.value.imagen,
      this.form.value.ingredientes
    );
    console.log(this.form);
  }

}
