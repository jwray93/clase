import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule, ModalController } from '@ionic/angular';

import { AgregarPageRoutingModule } from './agregar-routing.module';

import { AgregarPage } from './agregar.page';
import { SharedComponent } from '../../shared/shared.component';
import { LocationPickerComponent } from '../../shared/pickers/location-picker/location-picker.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    AgregarPageRoutingModule,
    LocationPickerComponent
  ],
  declarations: [AgregarPage]
})
export class AgregarPageModule {}
