export interface receta{
    id: number;
    title: string;
    description: string;
    ingredients: string[];
    image: string;
}
export class receta implements receta{
  constructor(
      public id: number,
      public title: string,
      public description: string,
      public ingredients: string[],
      public image: string
  ){}
}