import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { SecondComponent } from './second/second.component';
import { DetallePageModule } from './recetas/detalle/detalle.module';
import { RecetasPage } from './recetas/recetas.page';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'recetas',
    pathMatch: 'full'
  },
  {
    path: 'second',
    component: SecondComponent
  },
  {
    path: 'calculadora',
    loadChildren: () => import('./calculadora/calculadora.module').then( m => m.CalculadoraPageModule)
  },
  {
    path: 'calcu',
    loadChildren: () => import('./calcu/calcu.module').then( m => m.CalcuPageModule)
  },
  {
    path: 'clase6',
    loadChildren: () => import('./clase6/clase6.module').then( m => m.Clase6PageModule)
  },
  {
    path: 'recetas',
    children: [
      {
        path: '',
        loadChildren: () => import('./recetas/recetas.module').then( m => m.RecetasPageModule)
      },
      {
        path: ':recetaId',
        loadChildren: () => import('./recetas/detalle/detalle.module').then (m => m.DetallePageModule)
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
