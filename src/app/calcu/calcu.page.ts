import { Component, OnInit } from '@angular/core';
import { Calcu } from './calcu.model';

@Component({
  selector: 'app-calcu',
  templateUrl: './calcu.page.html',
  styleUrls: ['./calcu.page.scss'],
})
export class CalcuPage implements OnInit {
  valorActual: string = '0';
  subtotal: number = 0;
  operacionActual: string;
  constructor() { }
  ngOnInit() {
  }
  agregar(numero: string){
    if (this.valorActual == '0'){
      this.valorActual = numero;
    }else{
      this.valorActual = this.valorActual + numero;
    }
  }
  operacion(operacion: string){
    switch (operacion) {
      case '+':
        this.subtotal = this.subtotal + parseInt(this.valorActual);
        break;
      case '-':
        this.subtotal = this.subtotal - parseInt(this.valorActual);
        break;
      case '/':
        this.subtotal = this.subtotal / parseInt(this.valorActual);
        break;
      case '*':
        this.subtotal = this.subtotal * parseInt(this.valorActual);
        break;
      default:
        console.log('entro');
        break;
    }
    console.log(this.subtotal);
    this.valorActual = '0';
    this.operacionActual = operacion;
  }
}
